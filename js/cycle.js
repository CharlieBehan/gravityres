Cycle = function (duration, interval) {
   this.duration = duration || 0; 
   this.interval = interval || 0;
   this.lastAdvance = 0;
};

Cycle.prototype = { 
   execute: function(sprite, time, fps) {
      if (this.lastAdvance === 0) {
         this.lastAdvance = time;
      }

      if (this.interval && sprite.artist.cellIndex === 0) {
         if (time - this.lastAdvance > this.interval) {
            sprite.artist.advance();
            this.lastAdvance = time;
         }
      }
      else if (time - this.lastAdvance > this.duration) {
         sprite.artist.advance();
         this.lastAdvance = time;
      }
   }
};
