
//Sprite Sheet Artist Constructor
SpriteSheetArtist = function (spritesheet, cells) {
   this.cells = cells;
   this.spritesheet = spritesheet;
   this.cellIndex = 0;
};

//Sprite Sheet Artist prototype
SpriteSheetArtist.prototype = {
   advance: function () {
      if (this.cellIndex == this.cells.length-1) {
         this.cellIndex = 0;
      }
      else {
         this.cellIndex++;
      }
   },
   
   draw: function (sprite, context) {
      var cell = this.cells[this.cellIndex];

      context.drawImage(this.spritesheet, 
      					cell.left, cell.top, cell.width, cell.height,
                        sprite.left, sprite.top, cell.width, cell.height);
   }
};

var SpriteAnimator = function (cells, duration, callback) {
   this.cells = cells;
   this.duration = duration || 1000;
   this.callback = callback;
};

SpriteAnimator.prototype = {
   start: function (sprite, reappear) {
      var originalCells = sprite.artist.cells,
          originalIndex = sprite.artist.cellIndex,
          self = this;

      sprite.artist.cells = this.cells;
      sprite.artist.cellIndex = 0;
      
      setTimeout(function() {
         sprite.artist.cells = originalCells;
         sprite.artist.cellIndex = originalIndex;

         sprite.visible = reappear;

         if (self.callback) {
            self.callback(sprite, self);
         }
      }, self.duration); 
   },
};

//Sprite Constructor
var Sprite = function (type, artist, behaviors) {
    this.type = type || '';
    this.artist = artist || undefined;
    this.behaviors = behaviors || [];

    this.left = 0;
    this.top = 0;
    this.width = 10;
    this.height = 10;
   	this.velocityX = 0;
	this.velocityY = 0;
	this.opacity = 1.0;
    this.visible = true;
    hOffset = 0;

    return this;
};

//Sprite prototype
Sprite.prototype = {
	draw: function (context) { //Calls draw method of the sprites artist if visible and has artist
     context.save();
     context.globalAlpha = this.opacity;
      
     if (this.artist && this.visible) {
        this.artist.draw(this, context);
     }
     context.restore();
	},

   update: function (context, time, fps) { //Calls execute method of all of the sprites behaviours
      for (var i = 0; i < this.behaviors.length; ++i) {
         this.behaviors[i].execute(this, context, time, fps);
      }
   }
};