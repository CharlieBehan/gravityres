//Gravity constructor
var Gravity = function () {
//Canvas/context
   this.canvas = document.getElementById('game-canvas'),
   this.context = this.canvas.getContext('2d'),

//Constants
   this.TOP_PLAYER_TRACK_Y = 3,
   this.BOTTOM_PLAYER_TRACK_Y = 365,

   this.TOP_TRACK_ID = 0,
   this.BOTTOM_TRACK_ID = 1,
   this.MID_TRACK_ID = 2,

   this.WALL_HEIGHT = 130,  
   this.WALL_STROKE_WIDTH = 5,
   this.WALL_STROKE_STYLE = 'rgb(0,0,0)',

   this.STARTING_PLAYER_LEFT = 50,

   this.STARTING_PLAYER_VELOCITY = 0,
   this.PLAYER_HEIGHT = 32,
   this.PLAYER_CELLS_HEIGHT  = 32,
   this.PLAYER_CELLS_WIDTH = 32,

   this.PLAYER_DIRECTION_DESCEND = 0,
   this.PLAYER_DIRECTION_ASCEND = 1,
   this.PLAYER_DIRECTION_MID = 2,

   this.EXPLOSION_CELLS_HEIGHT = 120,
   this.EXPLOSION_DURATION = 500,

   this.BACKGROUND_VELOCITY = 60,

   this.WALL_VELOCITY_MULTIPLIER = 3,
   this.PILLAR_VELOCITY_MULTIPLIER = 1.5,

   this.STARTING_BACKGROUND_OFFSET = 0,
   this.STARTING_WALL_OFFSET = 0,
   this.STARTING_PILLAR_OFFSET = 0,

   this.PAUSE_CHECK_INTERVAL = 200,
   this.DEFAULT_TOAST_TIME = 1000,

//Pause
   this.paused = false,
   this.pauseStartTime = 0,
   this.totalTimePaused = 0,

   this.windowHasFocus = true,

   this.ascendStartTime = 0,
   this.descendStartTime = 0,
   this.deltaY = 0

//Tracks
   this.BOTTOM_WALL_TRACK_Y = 270,
   this.TOP_WALL_TRACK_Y = 0,

   this.playerTrack = this.BOTTOM_TRACK_ID,
   this.playerDirection = this.PLAYER_DIRECTION_MID,

//Images
   this.background1  = new Image(),
   this.background2  = new Image(),
   this.background3  = new Image(),
   this.backgroundPillars  = new Image(),
   this.spritesheet = new Image(),

//Animation/time
   this.lastAnimationFrameTime = 0,
   this.lastFpsUpdateTime = 0,
   this.fps = 60,

//Elements
   this.fpsElement = document.getElementById('fps'),
   this.toast = document.getElementById('toast'),
   this.scoreElement = document.getElementById('score'),
   this.pauseMenu = document.getElementById('pauseMenu'),
   this.highscoreElement = document.getElementById('highscore'),

   this.score = 0;
   this.highscore = 0;

//Offsets
   this.backgroundOffset = this.STARTING_BACKGROUND_OFFSET,
   this.spriteOffset = this.STARTING_BACKGROUND_OFFSET,
   this.pillarOffset = this.STARTING_PILLAR_OFFSET,

//Velocity
   this.bgVelocity = this.BACKGROUND_VELOCITY,
   this.pillarVelocity,
   this.wallVelocity,

//Walls
   this.wallData = [  // One screen for now
   // Level 1
      {
         left:      405,
         width:     230,
         height:    this.WALL_HEIGHT,
         fillStyle: 'yellow',
         track:     2,
      },

      {  left:      780,
         width:     100,
         height:    this.WALL_HEIGHT,
         fillStyle: 'yellow',
         track:     1,
      },

      {  left:      1100,
         width:     100,
         height:    this.WALL_HEIGHT,
         fillStyle: 'yellow',
         track:     2,
      },

      {  left:      1400,
         width:     180,
         height:    this.WALL_HEIGHT,
         fillStyle: 'yellow',
         track:     1,
      },

      {  left:      1750,
         width:     100,
         height:    this.WALL_HEIGHT,
         fillStyle: 'yellow',
         track:     2,
      },

      {  left:      1900,
         width:     100,
         height:    this.WALL_HEIGHT,
         fillStyle: 'yellow',
         track:     2,
      },

      {  left:      2100,
         width:     100,
         height:    this.WALL_HEIGHT,
         fillStyle: 'yellow',
         track:     1,
      },

      {  left:      2300,
         width:     100,
         height:    this.WALL_HEIGHT,
         fillStyle: 'yellow',
         track:     2,
      },

      {  left:      2500,
         width:     180,
         height:    this.WALL_HEIGHT,
         fillStyle: 'yellow',
         track:     1,
      },

      {  left:      2850,
         width:     100,
         height:    this.WALL_HEIGHT,
         fillStyle: 'yellow',
         track:     2,
      },

      {  left:      3000,
         width:     100,
         height:    this.WALL_HEIGHT,
         fillStyle: 'yellow',
         track:     2,
      },

      {  left:      3200,
         width:     100,
         height:    this.WALL_HEIGHT,
         fillStyle: 'yellow',
         track:     1,
      },

   //Level 2
      {  left:      3400,
         width:     70,
         height:    this.WALL_HEIGHT,
         fillStyle: 'orange',
         track:     2,
      },

      {  left:      3550,
         width:     80,
         height:    this.WALL_HEIGHT,
         fillStyle: 'orange',
         track:     1,
      },

      {  left:      3800,
         width:     100,
         height:    this.WALL_HEIGHT,
         fillStyle: 'orange',
         track:     2,
      },

      {  left:      4000,
         width:     100,
         height:    this.WALL_HEIGHT,
         fillStyle: 'orange',
         track:     2,
      },

      {  left:      4200,
         width:     50,
         height:    this.WALL_HEIGHT,
         fillStyle: 'orange',
         track:     1,
      },

      {  left:      4400,
         width:     80,
         height:    this.WALL_HEIGHT,
         fillStyle: 'orange',
         track:     2,
      },

      {  left:      4600,
         width:     100,
         height:    this.WALL_HEIGHT,
         fillStyle: 'orange',
         track:     1,
      },

      {  left:      4770,
         width:     100,
         height:    this.WALL_HEIGHT,
         fillStyle: 'orange',
         track:     2,
      },

      {  left:      4950,
         width:     50,
         height:    this.WALL_HEIGHT,
         fillStyle: 'orange',
         track:     1,
      },

      {  left:      5130,
         width:     80,
         height:    this.WALL_HEIGHT,
         fillStyle: 'orange',
         track:     2,
      },

      {  left:      5280,
         width:     100,
         height:    this.WALL_HEIGHT,
         fillStyle: 'orange',
         track:     1,
      },

      //Level 3
      {  left:      5400,
         width:     20,
         height:    this.WALL_HEIGHT,
         fillStyle: 'red',
         track:     1,
      },

      {  left:      5640,
         width:     50,
         height:    this.WALL_HEIGHT,
         fillStyle: 'red',
         track:     2,
      },

      {  left:      5760,
         width:     20,
         height:    this.WALL_HEIGHT,
         fillStyle: 'red',
         track:     1,
      },

      {  left:      5810,
         width:     10,
         height:    this.WALL_HEIGHT,
         fillStyle: 'red',
         track:     2,
      },

      {  left:      5890,
         width:     20,
         height:    this.WALL_HEIGHT,
         fillStyle: 'red',
         track:     1,
      },

      {  left:      6010,
         width:     10,
         height:    this.WALL_HEIGHT,
         fillStyle: 'red',
         track:     2,
      },

      {  left:      6200,
         width:     13,
         height:    this.WALL_HEIGHT,
         fillStyle: 'red',
         track:     1,
      },

      {  left:      6250,
         width:     10,
         height:    this.WALL_HEIGHT,
         fillStyle: 'red',
         track:     2,
      },

      {  left:      6400,
         width:     50,
         height:    this.WALL_HEIGHT,
         fillStyle: 'red',
         track:     2,
      },

      {  left:      6500,
         width:     20,
         height:    this.WALL_HEIGHT,
         fillStyle: 'red',
         track:     1,
      },

      {  left:      6600,
         width:     13,
         height:    this.WALL_HEIGHT,
         fillStyle: 'red',
         track:     1,
      },

      {  left:      6630,
         width:     10,
         height:    this.WALL_HEIGHT,
         fillStyle: 'red',
         track:     2,
      },

      {  left:      6700,
         width:     50,
         height:    this.WALL_HEIGHT,
         fillStyle: 'red',
         track:     2,
      },

      {  left:      6800,
         width:     20,
         height:    this.WALL_HEIGHT,
         fillStyle: 'red',
         track:     1,
      },
   ];
//Sprite Cells
   this.playerCells = [
      { left: 3,   top: 135, width: this.PLAYER_CELLS_WIDTH,
                             height: this.PLAYER_CELLS_HEIGHT },

      { left: 39,  top: 135, width: this.PLAYER_CELLS_WIDTH, 
                             height: this.PLAYER_CELLS_HEIGHT },

      { left: 76,  top: 135, width: this.PLAYER_CELLS_WIDTH, 
                             height: this.PLAYER_CELLS_HEIGHT },

      { left: 112, top: 135, width: this.PLAYER_CELLS_WIDTH, 
                             height: this.PLAYER_CELLS_HEIGHT },

      { left: 148, top: 135, width: this.PLAYER_CELLS_WIDTH, 
                             height: this.PLAYER_CELLS_HEIGHT }
   ],

   this.explosionCells = [
      { left: 1,   top: 48, width: 50, height: this.EXPLOSION_CELLS_HEIGHT },
      { left: 60,  top: 48, width: 68, height: this.EXPLOSION_CELLS_HEIGHT },
      { left: 143, top: 48, width: 68, height: this.EXPLOSION_CELLS_HEIGHT },
      { left: 230, top: 48, width: 68, height: this.EXPLOSION_CELLS_HEIGHT },
      { left: 305, top: 48, width: 68, height: this.EXPLOSION_CELLS_HEIGHT },
      { left: 389, top: 48, width: 68, height: this.EXPLOSION_CELLS_HEIGHT },
      { left: 470, top: 48, width: 68, height: this.EXPLOSION_CELLS_HEIGHT }
   ],

   this.walls    = [],

   this.wallArtist = {
      draw: function (sprite, context) {
         var top;
         
         context.save();

         top = gravity.calculateWallTop(sprite.track);

         context.lineWidth = gravity.WALL_STROKE_WIDTH;
         context.strokeStyle = gravity.WALL_STROKE_STYLE;
         context.fillStyle = sprite.fillStyle;

         context.strokeRect(sprite.left, top, sprite.width, sprite.height);
         context.fillRect  (sprite.left, top, sprite.width, sprite.height);

         context.restore();
      }
   },

   this.playerArtist = new SpriteSheetArtist(this.spritesheet, this.playerCells),

   this.collideBehavior = {
      execute: function (sprite, time, fps, context) {
         var otherSprite;

         for (var i=0; i < gravity.sprites.length; ++i) { 
            otherSprite = gravity.sprites[i];
            if (this.isCandidateForCollision(sprite, otherSprite)) {
               if (this.didCollide(sprite, otherSprite, context)) {
                  console.log('YOU COLLIDED WITH WALL '+i); 
                  this.processCollision(sprite, otherSprite);
               }
            }
         }
      },

      isCandidateForCollision: function (sprite, otherSprite) {
         return sprite !== otherSprite && !sprite.exploding;
      }, 

      didRunnerCollideWithOtherSprite: function (left, top, right, bottom,
                                                 centerX, centerY,
                                                 otherSprite, context) {

         context.beginPath();
         context.rect(otherSprite.left - gravity.spriteOffset, otherSprite.top,
                      otherSprite.width, otherSprite.height);

         return context.isPointInPath(left, top)        ||
                context.isPointInPath(right, top)       ||

                context.isPointInPath(centerX, centerY) ||

                context.isPointInPath(left, bottom)     ||
                context.isPointInPath(right, bottom);
      },
     
      didCollide: function (sprite, otherSprite, context) {
         var left = sprite.left+10,
             right = sprite.left + sprite.width+20,
             top = sprite.top,
             bottom = sprite.top + sprite.height,
             centerX = left + sprite.width/2,
             centerY = sprite.top + sprite.height/2;

            return this.didRunnerCollideWithOtherSprite(left, top, right, bottom,
                                                  centerX, centerY,
                                                  otherSprite, context);
      },

      processCollision: function (sprite, otherSprite) {
         if ('wall' === otherSprite.type) {
            gravity.explode(sprite);
         }
      }
   };

   this.jumpBehaviour = 
   {
      pause: function(sprite, now)
      {
         if(sprite.ascendTimer.isRunning())
         {
            sprite.ascendTimer.pause(now);
         }
         else if(sprite.descendTimer.isRunning())
         {
            sprite.descendTimer.pause(now);
         }
      },

      unpause: function(sprite, now)
      {
         if(sprite.ascendTimer.isRunning())
         {
            sprite.ascendTimer.unpause(now);
         }
         else if(sprite.descendTimer.isRunning())
         {
            sprite.descendTimer.unpause(now);
         }
      },

      isAscending: function(sprite)
      {
         return sprite.ascendTimer.isRunning();
      },

      ascend: function(sprite, now)
      {
         var elapsed = sprite.ascendTimer.getElapsedTime(now),
             deltaY = elapsed / (sprite.JUMP_DURATION) * sprite.JUMP_HEIGHT;
         sprite.top = sprite.verticalLaunchPosition - deltaY;
      },

      isDoneAscending: function(sprite, now)
      {
         return sprite.top >= sprite.TOP_PLAYER_TRACK_Y;
      },

      finishAscent: function(sprite, now)
      {
         sprite.ascendTimer.stop(now);
         this.playerDirection = this.PLAYER_DIRECTION_MID;
         this.playerTrack = this.TOP_TRACK_ID;
         sprite.velocityX = gravity.STARTING_PLAYER_VELOCITY;
         sprite.top = this.TOP_PLAYER_TRACK_Y;
      },

      isDescending: function(sprite)
      {
         return sprite.descendTimer.isRunning();
      },

      descend: function(sprite, now)
      {
         var elapsed = sprite.descendTimer.getElapsedTime(now),
         deltaY = elapsed / (sprite.JUMP_DURATION) * sprite.JUMP_HEIGHT;
         sprite.top = sprite.jumpApex + deltaY;
      },

      isDoneDescending: function(sprite, now)
      {
         return sprite.top + sprite.height <= gravity.BOTTOM_PLAYER_TRACK_Y;
      },

      finishDescent: function(sprite, now)
      {
         sprite.stopJumping();
         this.playerDirection = this.PLAYER_DIRECTION_MID;
         this.playerTrack = this.BOTTOM_TRACK_ID;
         sprite.velocityX = gravity.STARTING_PLAYER_VELOCITY;
         sprite.top = this.BOTTOM_PLAYER_TRACK_Y;
      },

      execute: function(sprite, now, fps, context, lastAnimationFrameTime)
      {
         if(!sprite.jumping)
         {
            return;
         }
         if(this.isAscending(sprite))
         {
            if(!this.isDoneAscending(sprite, now))
            {
               this.ascend(sprite, now);
            }
            else
               this.finishAscent(sprite, now);
         }
         else if(this.isDescending(sprite))
         {
            if(!this.isDoneDescending(sprite, now))
            {
               this.descend(sprite, now);
            }
            else
               this.finishDescent(sprite, now);
         }
      }
   };



   this.player = new Sprite('player', this.playerArtist, [ new Cycle(100, 10), this.collideBehavior ]);

   this.sprites = [ this.player ];  //Array for sprites

   this.explosionAnimator = new SpriteAnimator(
      this.explosionCells,        
      this.EXPLOSION_DURATION, 

      function (sprite, animator) { 
         sprite.exploding = false;
         sprite.artist.cellIndex = 0;
      }
   );
};

Gravity.prototype = {
// Drawing
   draw: function (now) {
      this.setWallVelocity();
      this.setPillarVelocity();
      this.setOffsets();

      this.drawBackground();
      this.drawPillars();

      this.updateSprites(now);
      this.drawSprites();
      this.movePlayer(now, this.playerDirection);
   },

   setWallVelocity: function () {
      this.wallVelocity = this.bgVelocity * this.WALL_VELOCITY_MULTIPLIER;
   },

   setPillarVelocity: function () {
      this.pillarVelocity = this.bgVelocity * this.PILLAR_VELOCITY_MULTIPLIER;
   },

   setOffsets: function () {
      this.setBackgroundOffset();
      this.setSpriteOffsets();
      this.setPillarOffset();
   },

   setBackgroundOffset: function () {
      var offset = this.backgroundOffset + this.bgVelocity/this.fps;

      if (offset > 0 && offset < this.background1.width*2) {
         this.backgroundOffset = offset;
      }
      else {
         this.backgroundOffset = 0;
      }
   },

   setPillarOffset: function () {
      var offset = this.pillarOffset + this.pillarVelocity/this.fps;

      if (offset > 0 && offset < this.backgroundPillars.width*2) {
         this.pillarOffset = offset;
      }
      else {
         this.pillarOffset = 0;
      }
   },

   setSpriteOffsets: function () {
      var i, sprite;
   
      this.spriteOffset += this.wallVelocity / this.fps; 

      for (i=0; i < this.sprites.length; ++i) {
         sprite = this.sprites[i];
      
         if ('player' !== sprite.type) {
            sprite.offset = this.spriteOffset; 
         }
      }
   },

   drawBackground: function () {
      this.context.save();
   
      this.context.globalAlpha = 1.0;
      this.context.translate(-this.backgroundOffset, 0);

      this.context.drawImage(this.background1, 0, 0, this.background1.width, this.background1.height);

      this.context.drawImage(this.background2, this.background1.width, 0, this.background1.width+1, this.background1.height);

      this.context.drawImage(this.background3, this.background1.width*2, 0, this.background1.width+1, this.background1.height);

      this.context.restore();
   },

   drawPillars: function () {
      this.context.save();
   
      this.context.globalAlpha = 1.0;
      this.context.translate(-this.pillarOffset, 0);

      this.context.drawImage(this.backgroundPillars, 0, 0, this.backgroundPillars.width, this.backgroundPillars.height);

      this.context.drawImage(this.backgroundPillars, this.backgroundPillars.width, 0, this.backgroundPillars.width+1, this.backgroundPillars.height);

      this.context.drawImage(this.backgroundPillars, this.backgroundPillars.width*2, 0, this.backgroundPillars.width+1, this.backgroundPillars.height);

      this.context.restore();
   },

   calculateFps: function (now) { 
      var fps = 1000/(now - this.lastAnimationFrameTime);
      this.lastAnimationFrameTime = now;

      if(now - this.lastFpsUpdateTime > 1000) { // Waits 1 second till it updates again
         this.lastFpsUpdateTime = now;
         this.fpsElement.innerHTML = fps.toFixed(0) + 'fps';
      }

      return fps;
   },

   calculateScore: function () { 
      ++this.score;
      this.scoreElement.innerHTML = (this.score/25).toFixed(0);

      return this.score;
   },

   calculateHighscore: function () { 
      if(this.score > this.highscore){
         this.highscore = this.score;
      }
      this.highscoreElement.innerHTML = 'Highscore: '+(this.highscore/25).toFixed(0);

      return this.highscore;
   },

   calculateWallTop: function (track) {
      var top;

      if      (track === 1) { top = this.BOTTOM_WALL_TRACK_Y; }
      else if (track === 2) { top = this.TOP_WALL_TRACK_Y; }

      return top;
   },

// Toast
   displayToast: function (text, length) {
      length = length || this.DEFAULT_TOAST_TIME;

      toast.style.display = 'block';
      toast.innerHTML = text;

      setTimeout( function (e) {
         if (gravity.windowHasFocus) {
            toast.style.opacity = 1.0; // After toast is displayed
         }
      }, 50);

      setTimeout( function (e) {
         if (gravity.windowHasFocus) {
            toast.style.opacity = 0; // Starts CSS3 transition
         }

         setTimeout( function (e) { 
            if (gravity.windowHasFocus) {
               toast.style.display = 'none'; 
            }
         }, 480);
      }, length);
   },

//Reset
   restartGame: function(){
      this.resetOffsets();
      this.resetPlayer();
      if( this.score > this.highscore ) {
         this.highscore = this.score;
      }
      this.score = 0;
   },

   resetOffsets: function () {
      this.bgVelocity = 60;
      this.backgroundOffset = 0;
      this.pillarOffset = 0;
      this.spriteOffset = 0;
   },

   resetPlayer: function() {
      this.player.velocityX = this.STARTING_PLAYER_VELOCITY;
      this.player.left = this.STARTING_PLAYER_LEFT;
      this.player.top = this.BOTTOM_PLAYER_TRACK_Y;
      this.playerTrack = this.BOTTOM_TRACK_ID,
      this.playerDirection = this.PLAYER_DIRECTION_MID,
      this.player.visible = true;
      this.player.exploding = false;
      this.player.artist.cells = this.playerCells;
      this.player.artist.cellIndex = 0;
   },

// Sprites
   startPlayer: function () {
      this.player.velocityX = this.STARTING_PLAYER_VELOCITY;
      this.player.left = this.STARTING_PLAYER_LEFT;
      this.player.top = this.BOTTOM_PLAYER_TRACK_Y;
      this.player.artist.cells = this.playerCells;
   },

   explode: function (sprite, silent) {
      sprite.exploding = true;
      this.explosionAnimator.start(sprite, true);
      gravity.displayToast('YOU LOSE', 500); 
      setTimeout(function (e){
         gravity.restartGame();
      }, 500);
   },

   createWallSprites: function () {
      var sprite, wd; 
   
      for (var i=0; i < this.wallData.length; ++i) {
         wd = this.wallData[i];
         sprite  = new Sprite('wall', this.wallArtist);

         sprite.left      = wd.left;
         sprite.width     = wd.width;
         sprite.height    = wd.height;
         sprite.fillStyle = wd.fillStyle;
         sprite.opacity   = wd.opacity;
         sprite.track     = wd.track;

         sprite.top = this.calculateWallTop(wd.track);
   
         this.walls.push(sprite);
      }
   },

   movePlayer: function (now, playerDirection) { //Checks what direction to send player
      if (playerDirection === this.PLAYER_DIRECTION_ASCEND){
         this.ascendPlayer(now, this.player);
      }
      else if (playerDirection === this.PLAYER_DIRECTION_DESCEND){
         this.descendPlayer(now, this.player);
      }
      else return;
   },

   ascendPlayer: function (now, sprite) {
      if (this.playerTrack == this.BOTTOM_TRACK_ID){   
         ascendStartTime = now;
         this.playerTrack = this.MID_TRACK_ID;
      }

      deltaY = (now-ascendStartTime) / 2;
      sprite.top = this.BOTTOM_PLAYER_TRACK_Y - deltaY;

      if (sprite.top <= this.TOP_PLAYER_TRACK_Y){   
         ascendStartTime = 0;
         this.playerDirection = this.PLAYER_DIRECTION_MID;
         this.playerTrack = this.TOP_TRACK_ID;
      }
   },

   descendPlayer: function (now, sprite) {
      if (this.playerTrack == this.TOP_TRACK_ID){   
         descendStartTime = now;
         this.playerTrack = this.MID_TRACK_ID;
      }

      deltaY = (now-descendStartTime) / 2;
      sprite.top = this.TOP_PLAYER_TRACK_Y + deltaY;

      if (sprite.top >= this.BOTTOM_PLAYER_TRACK_Y){   
         descendStartTime = 0;
         this.playerDirection = this.PLAYER_DIRECTION_MID;
         this.playerTrack = this.BOTTOM_TRACK_ID;
      }
   },

// Pause
   togglePaused: function () {
      var now = +new Date();
      this.paused = !this.paused;

      if (this.paused) {
         this.pauseStartTime = now;
         gravity.displayToast('Paused', 500); // displays 'Paused' for half of a second
		 gravity.displayPauseMenu();
      }
      else {
         this.lastAnimationFrameTime += (now - this.pauseStartTime);
         gravity.displayToast('Unpaused', 500); // displays 'Unpaused' for half of a second
		 gravity.removePauseMenu();
      }
   },
   
   displayPauseMenu: function()
   {
      pauseMenu.style.display = 'block'; // Activate pause menu element

      setTimeout( function (e) {
         if (gravity.windowHasFocus) {
            pauseMenu.style.opacity = 0.5; // After pause menu is displayed
         }
      }, 50);  
   },

	removePauseMenu: function()
   {
       var length = 100;
      setTimeout( function (e) {
         if (gravity.windowHasFocus) {
            pauseMenu.style.opacity = 0; // Starts CSS3 transition
         }
         setTimeout( function (e) { 
            if (gravity.windowHasFocus) {
            pauseMenu.style.display = 'none'; 
          }
       }, 480);
      }, length);
   },
// Animation
   animate: function (now) { // Game loop
      if (gravity.paused){
         setTimeout(function() {
            requestNextAnimationFrame(gravity.animate);
         }, gravity.PAUSE_CHECK_INTERVAL);
      }
      else {
         gravity.fps = gravity.calculateFps(now);
         gravity.score = gravity.calculateScore();
         gravity.highscore = gravity.calculateHighscore();
         gravity.draw(now);
         requestNextAnimationFrame(gravity.animate);
      }
   },

// Initialization
   initializeImages: function () { // loads images and runs startGame() once images are loaded
      this.background1.src = 'images/background_level_one_yellow.png';
      this.background2.src = 'images/background_level_one_orange.png';
      this.background3.src = 'images/background_level_one_red.png';
      this.backgroundPillars.src = 'images/background_pillars.png';
      this.spritesheet.src = 'images/spritesheet.png';

      this.background1.onload = function (e) {
         gravity.startGame();
      };
   },

   start: function () {
      this.createSprites();
      this.initializeImages();
      this.startPlayer();
      gravity.displayToast('Avoid the walls by pressing<br>SPACE', 3000);
   },

   startGame: function () {
      requestNextAnimationFrame(this.animate);
   },

   addSpritesToSpriteArray: function () {
      for (var i=0; i < this.walls.length; ++i) {
         this.sprites.push(this.walls[i]);
      }
   },

   updateSprites: function (now) {
      var sprite;
   
      for (var i=0; i < this.sprites.length; ++i) {
         sprite = this.sprites[i];
         if (sprite.visible && this.spriteInView(sprite)) {
            sprite.update(now, this.fps, this.context);
         }
      }
   },
   
   drawSprites: function() {
      var sprite;
   
      for (var i=0; i < this.sprites.length; ++i) {
         sprite = this.sprites[i];
   
         if (sprite.visible && this.spriteInView(sprite)) {
            this.context.translate(-sprite.offset, 0);

            sprite.draw(this.context);

            this.context.translate(sprite.offset, 0);
         }
      }
   },
   
   spriteInView: function(sprite) {
      return sprite === this.player || 
         (sprite.left + sprite.width > this.spriteOffset &&
          sprite.left < this.spriteOffset + this.canvas.width);   
   },

   createSprites: function() {  
      this.createWallSprites();
      this.addSpritesToSpriteArray();
   },
};

window.onkeydown = function (e) { // when a key is pressed it checks if it is 'p'
   var key = e.keyCode;           // if so pauses the game

   if (key === 80 || (gravity.paused && key !== 80)) {  // 'p'
      gravity.togglePaused();
   }
	if (key === 82) { // 'r'
      //gravity.togglePaused();
      gravity.restartGame(); ///////////////////////////////////////////WIP///////////////////////WIP//////////////////
      
       // restarts game when R is pressed
   }
   if (key === 32) {  // 'SPACE'
      if(gravity.playerTrack === gravity.BOTTOM_TRACK_ID) { // If on bottom track ascend to top
         gravity.playerDirection = gravity.PLAYER_DIRECTION_ASCEND;
         gravity.movePlayer(gravity.playerDirection);
      }
      else if(gravity.playerTrack === gravity.TOP_TRACK_ID) { // if on top track descend to bottom
         gravity.playerDirection = gravity.PLAYER_DIRECTION_DESCEND;
         gravity.movePlayer(gravity.playerDirection);
      }
   }
};

window.onblur = function (e) {  // pauses when looking at different window
   gravity.windowHasFocus = false;
   
   if (!gravity.paused) {
      gravity.togglePaused();
   }
};

window.onfocus = function (e) {  // unpauses when go back to the window
   gravity.windowHasFocus = true;

   if (gravity.paused) {
      gravity.displayToast('3', 500); // displays '3' for half of a second

      setTimeout(function (e) {
         gravity.displayToast('2', 500);

         setTimeout(function (e) {
            gravity.displayToast('1', 500);

            setTimeout(function (e) {
               if ( gravity.windowHasFocus) {
                  gravity.togglePaused();
               }
            }, gravity.DEFAULT_TOAST_TIME);
         }, gravity.DEFAULT_TOAST_TIME);
      }, gravity.DEFAULT_TOAST_TIME);
   }
};

// Launch game
   var gravity = new Gravity();
   gravity.start();

